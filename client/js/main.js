const Phaser = require("phaser");
const { io } = require("socket.io-client");

class GameScene extends Phaser.Scene {
    constructor() {
        super("game");
    }

    preload() {}

    create() {
        // Draw the grid
        const GRID_WIDTH = 200; // 10 * 20
        const GRID_HEIGHT = 200;
        const CELL_SIZE = 50;

        for (let x = GRID_WIDTH; x >= 0; x -= CELL_SIZE) {
            this.add.line(x, 0, x, 0, x, -(GRID_HEIGHT * 2), 0xffffff).setOrigin(0, 0);
        }
        for (let y = GRID_HEIGHT; y >= 0; y -= CELL_SIZE) {
            this.add.line(0, -y, 0, -y, GRID_WIDTH * 2, -y, 0xffffff).setOrigin(0, 0);
        }

        this.objects = new Map();
        this.activePlayer = this.add.circle(0, 0, 20, 0xffffff);
        this.cameras.main.startFollow(this.activePlayer);

        // Set up movement
        this.movement = {
            up: false,
            down: false,
            left: false,
            right: false
        };
        this.addKey("W", "up");
        this.addKey("S", "down");
        this.addKey("A", "left");
        this.addKey("D", "right");
        this.inputsDirty = false; // Indicates whether inputs have changed and need to be sent to the server

        // The tick method runs every 30 milliseconds (33.3 times per second),
        // sending input events when necessary.
        setInterval(this.tick.bind(this), 30);

        // Connect to the server
        this.socket = io();

        this.socket.on("connect", () => {
            console.log("Connected!");
        });

        // Deletes old objects when disconnected
        this.socket.on("disconnect", () => {
            console.log("Disconnected");
            for (const [key, object] of this.objects) {
                if (object !== this.activePlayer) object.destroy();
                this.objects.delete(key);
            }
        });

        // Sets the ID of the active player
        this.socket.on("activePlayerID", id => {
            this.objects.set(id, this.activePlayer);
        });

        // Adds a new object to the world
        this.socket.on("newObject", netData => {
            let object;
            const x = netData.position.x * 20, y = 20 - (netData.position.y * 20); // Y-axis inverted; 20x scale factor
            switch (netData.type) {
                case 0: {
                    object = this.add.circle(x, y, 20, 0xffffff);
                    break;
                }
                case 1: {
                    object = this.add.circle(x, y, 20, 0xff0000);
                    break;
                }
                case 2: {
                    object = this.add.rectangle(x, y, 40, 40, 0x0000ff);
                    break;
                }
            }
            this.objects.set(netData.id, object);
        });

        // Updates the position of an existing object
        this.socket.on("updateObject", (id, position) => {
            if (this.objects.has(id)) {
                const object = this.objects.get(id);
                object.setPosition(position.x * 20, 20 - (position.y * 20));
            } else {
                console.warn(`Object with ID ${id} not found`);
            }
        });

        // Deletes an object from the world
        this.socket.on("deleteObject", id => {
            if (this.objects.has(id)) {
                console.log(this.objects.get(id));
                this.objects.get(id).destroy();
                this.objects.delete(id);
            } else {
                console.warn(`Object with ID ${id} not found`);
            }
        });
    }

    addKey(keyCode, valueToToggle) {
        const key = this.input.keyboard.addKey(keyCode);
        key.on("down", () => {
            this.movement[valueToToggle] = true;
            this.inputsDirty = true;
        });
        key.on("up", () => {
            this.movement[valueToToggle] = false;
            this.inputsDirty = true;
        });
    }

    tick() {
        if (this.inputsDirty) {
            this.inputsDirty = false;
            this.socket.emit("input", this.movement);
        }
    }

    update() {}
}

new Phaser.Game({
    width: 800,
    height: 600,
    type: Phaser.AUTO,
    backgroundColor: 0x000000,
    scene: GameScene
});
