const express = require("express");
const http = require("http");
const { world, players, objects, newObjects, deletedObjects} = require("./game");
const Player = require("./player");

const app = express();
const server = http.createServer(app);
const io = require("socket.io")(server);

app.use(express.static("client"));

io.on("connection", socket => {
    console.log("Player connected");
    const player = new Player(socket);
    players.add(player);
    objects.add(player);
    newObjects.add(player);
    socket.emit("activePlayerID", player.netData.id);
    for (const object of objects) {
        if (object !== player) socket.emit("newObject", object.netData);
    }

    socket.on("disconnect", () => {
        console.log("Player disconnected");
        world.destroyBody(player.body);
        players.delete(player);
        objects.delete(player);
        deletedObjects.add(player);
    });

    socket.on("input", movement => { player.movement = movement; });
})

server.listen(8000, () => {
    console.log("phaser-planck-example v0.1.0");
    console.log("Listening on 0.0.0.0:8000");
    console.log("Press Ctrl+C to exit.");
});
