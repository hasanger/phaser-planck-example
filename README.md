# phaser-planck-example

A game template built using Phaser 3, Planck.js, and Socket.IO.

## Setup

[Node.js](https://nodejs.org) and [Git](https://git-scm.com) are required. To set up the project, run the following commands:
```
git clone https://gitlab.com/hasanger/phaser-planck-example.git
cd phaser-planck-example
npm install
npm run buildClient
npm start
```

To open the game, go to http://0.0.0.0:8000 in your browser.
